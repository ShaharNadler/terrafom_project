# Terrafom project
~~~
1. Create a Resource Group for your Project
2. Create a Virtual Network with two subnets: the “public” and the “private” subnets
3. Deploy the web server into the public subnet and allow users to reach the application from
   everywhere
4. Deploy the database server into the private subnet and ensure that there is no public access to
   the database (only from the application)
5. Configure the database
6. Configure the web server
7. Ensure the application is up and running (and work automatically after reboot)
~~~
![Example Image](diagram1.png)

## Notes:
~~~
1. Generate ssh_keys for the web & db servers
2. Change the db_password, web_ssh_public_key, db_ssh_public_key in the terraform.tfvars to yours
3. Put the public ip of the web server in the URL with /data to get the data of the table and choose the Post method 
   in postman to post tada to the table.
~~~
## Run the following commands:
~~~
terraform init
terraform plan
terraform apply 
~~~

## Link to a Gitlab repository with this task in azure + how to do it with a Load-Balancer
~~~
https://github.com/nadler1309/weightTracker_shachar/tree/feature_simple_net
~~~

